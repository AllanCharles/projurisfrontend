import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { LocalStorageService } from '../services/storage.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {
  pokemons: any[] = [];
  page = 1;
  totalPokemons: number = 1;
  pokemonFavorito: string = 'undefined';

  constructor(
    private dataService: DataService,
    private localStorageService: LocalStorageService

  ) { }

  // ngOnInit(): void {
  //   this.dataService.getPokemons().subscribe((reponse: any)=>{
  //     console.log(reponse)
  //   });
  // }

  ngOnInit(): void {
    this.getPokemons()
    this.pokemonFavorito = this.localStorageService.get('nome_pokemon')
  }

  


  onClickFavoritar(idPokemon: number, nomePokemon: string) {
    this.localStorageService.clear()
    this.localStorageService.set('id_pokemon', idPokemon)
    this.localStorageService.set('nome_pokemon', nomePokemon)
    this.load()
  }

  load() {
    location.reload()
  }



  getPokemons() {
    this.dataService.getPokemons(10, this.page + 0).subscribe((response: any) => {
      this.totalPokemons = response.count;
      response.results.forEach(result => {
        this.dataService.getMoreData(result.name)
          .subscribe((uniqResponse: any) => {
            this.pokemons.push(uniqResponse)
            // console.log(this.pokemons)
          });
      });
    });
  }

}
